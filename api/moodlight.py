# Copyright 2018 Haute école d'ingénierie et d'architecture de Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unicornhat as uh
from flask import Flask, request

COLUMNS = 8
ROWS = 4
WHITE_COLOR = (255, 255, 255)

app = Flask(__name__)
uh.set_layout(uh.PHAT)
uh.brightness(1.0)

# int_or_zero converts a string(s) to an integer between 0 and 255
# If the string is not a valid integer, it returns 0.
def int_or_zero(s):
    try:
        i = int(s)
        if i < 0:
            return 0
        if i > 255:
            return 255
        return i
    except ValueError:
        return 0

# color converts 3 strings (r, g, b) to a tuple of integers representing the color.
def color(r, g, b):
    red = int_or_zero(r)
    green = int_or_zero(g)
    blue = int_or_zero(b)
    # If no color, then WHITE_COLOR
    if red + green + blue == 0:
        return WHITE_COLOR
    return (red, green, blue)

def light_on(color):
    for y in range(ROWS):
        for x in range(COLUMNS):
            uh.set_pixel(x, y, *color)
    uh.show()

def light_off():
    uh.off()

@app.route("/on")
def switch_on():
    light_on(color(
        request.args.get("r", "0"),
        request.args.get("g", "0"),
        request.args.get("b", "0")))
    return "OK, lamp switched on."

@app.route("/off")
def switch_off():
    light_off()
    return "OK, lamp switched off"
